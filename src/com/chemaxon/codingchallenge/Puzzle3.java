package com.chemaxon.codingchallenge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Puzzle3 {

    public static void main(String[] args) {
        try {
            String fileName = "/Users/honza/Downloads/coding-challenge-2021/input3.txt";

            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String line;
                int count = 0;
                while ((line = br.readLine()) != null) {
                    String line2 = br.readLine();
                    String line3 = br.readLine();

                    String[] split1 = line.trim().split("\\s+");
                    String[] split2 = line2.trim().split("\\s+");
                    String[] split3 = line3.trim().split("\\s+");

                    for (int i = 0; i < 3; i++) {
                        int a = Integer.valueOf(split1[i]);
                        int b = Integer.valueOf(split2[i]);
                        int c = Integer.valueOf(split3[i]);

                        if (a + b > c && a + c > b && b + c > a) {
                            count++;
                        }
                    }


                }
                System.out.println(count);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}