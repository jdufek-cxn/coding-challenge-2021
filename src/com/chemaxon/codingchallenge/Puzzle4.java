package com.chemaxon.codingchallenge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Puzzle4 {

    public static void main(String[] args) {
        try {
            String fileName = "/Users/honza/Downloads/coding-challenge-2021/input4.txt";

            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String line;

                Integer sum = 0;
                while ((line = br.readLine()) != null) {

                    int indexOfName = line.lastIndexOf("-");
                    int indexOfId = line.lastIndexOf("[");
                    String name =  line.substring(0, indexOfName);
                    Integer id = Integer.valueOf(line.substring(indexOfName + 1, indexOfId));
                    String checksum = line.substring(indexOfId + 1, line.length() - 1);

                    System.out.println(decode(name, id));

                }
                System.out.println(sum);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String decode(String name, Integer id) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (c == '-') {
                sb.append(" ");
                continue;
            }
            int result = c + id;
            while (result > 122) {
                result -= 26;
            }
            sb.append((char) result);
        }
        return sb.toString();
    }

    private static boolean check(String n, String checksum) {
        Map<Character, Integer> occrs = new HashMap<>();
        String name = n.replaceAll("-", "");
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (occrs.containsKey(c)) {
                occrs.put(c, occrs.get(c) + 1);
            } else {
                occrs.put(c, 1);
            }
        }
        String collect = occrs.entrySet().stream().sorted((o1, o2) -> {
            if (o1.getValue() == o2.getValue()) {
                return o1.getKey().compareTo(o2.getKey());
            }
            return o2.getValue() - o1.getValue();
        }).limit(5).map(entry -> entry.getKey().toString()).collect(Collectors.joining());
        return checksum.equals(collect);
    }
}