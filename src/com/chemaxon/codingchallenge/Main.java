package com.chemaxon.codingchallenge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            String fileName = "/Users/honza/Downloads/coding-challenge-2021/input1.txt";

            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }
            }
            // String[] tokens = reader.readLine().split(",");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}