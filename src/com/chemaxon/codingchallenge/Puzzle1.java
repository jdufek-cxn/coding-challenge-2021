package com.chemaxon.codingchallenge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Puzzle1 {

    public static void main(String[] args) {
        try {
            String fileName = "/Users/honza/Downloads/coding-challenge-2021/input1.txt";

            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String[] instructions = br.readLine().split(", ");
                Solver solver = new Solver();
                for (String instruction :
                        instructions) {

                    Point p = solver.instruct(instruction);
                    if (p != null) {
                        System.out.println("Distance of " + p.posX + ", " + p.posY + ":");
                        System.out.println(Math.abs(p.posX) + Math.abs(p.posY));
                        return;
                    }

                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class Solver {

        private Integer curX = 0;
        private Integer curY = 0;
        private Integer currentDirection = 0;

        private Set<Point> visitedPoints = new HashSet<>();

        public Solver() {
            visitedPoints.add(new Point(0, 0));
        }

        private Point instruct(String instruction) {

            char direction = instruction.charAt(0);
            Integer steps = Integer.valueOf(instruction.substring(1));

            if (direction == 'R') {
                currentDirection++;
            } else {
                currentDirection--;
                if (currentDirection < 0) {
                    currentDirection += 4;
                }
            }
            currentDirection = currentDirection % 4;

            switch (currentDirection) {
                case 0:
                    for (int i = 1; i <= steps; i++ ) {
                        Point p = new Point(curX, curY + i);
                        if (checkOrAdd(p)) {
                            return p;
                        }
                    }
                    curY += steps;
                    break;
                case 1:
                    for (int i = 1; i <= steps; i++ ) {
                        Point p = new Point(curX + i, curY);
                        if (checkOrAdd(p)) {
                            return p;
                        }
                    }
                    curX += steps;
                    break;
                case 2:
                    for (int i = 1; i <= steps; i++ ) {
                        Point p = new Point(curX, curY - i);
                        if (checkOrAdd(p)) {
                            return p;
                        }
                    }
                    curY -= steps;
                    break;
                case 3:
                    for (int i = 1; i <= steps; i++ ) {
                        Point p = new Point(curX - i, curY);
                        if (checkOrAdd(p)) {
                            return p;
                        }
                    }
                    curX -= steps;
                    break;
                default:
                    throw new RuntimeException();
            }
            return null;
        }

        private boolean checkOrAdd(Point p) {
            if (visitedPoints.contains(p)) {
                return true;
            }
            visitedPoints.add(p);
            return false;
        }

        private Integer getDistance() {
            return Math.abs(curX) + Math.abs(curY);
        }
    }

    private static class Point {
        public Point(int posX, int posY) {
            this.posX = posX;
            this.posY = posY;
        }

        private int posX;
        private int posY;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return posX == point.posX && posY == point.posY;
        }

        @Override
        public int hashCode() {
            return Objects.hash(posX, posY);
        }
    }
}