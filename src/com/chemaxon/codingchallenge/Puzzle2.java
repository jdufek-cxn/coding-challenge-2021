/*
 * Copyright (c) 1998-2021 ChemAxon Ltd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ChemAxon. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms of the agreements
 * you entered into with ChemAxon.
 */

package com.chemaxon.codingchallenge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Puzzle2 {

    public static void main(String[] args) {
        try {
            String fileName = "/Users/honza/Downloads/coding-challenge-2021/input2.txt";

            try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String line;
                Solver s = new Solver();
                while ((line = br.readLine()) != null) {
                    System.out.print(s.solveLine(line));
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static class Solver {

        private static final String[][] MATRIX = {
                { "", "", "1", "", "" },
                { "", "2", "3", "4", ""},
                {"5", "6", "7", "8", "9"},
                {"", "A", "B", "C", ""},
                {"", "", "D", "", ""}
        };

        // private int curNmr = 5;
        private int curPosX = 0;
        private int curPosY = 2;

        private String solveLine(String line) {
            for (int i = 0; i < line.length(); i++) {
                char dir = line.charAt(i);
                move(dir);
            }
            return MATRIX[curPosY][curPosX];
        }

        private void move (char dir) {
            int newPosX, newPosY;
            switch (dir) {
                case 'U':
                    newPosX = curPosX;
                    newPosY = curPosY - 1;
                    break;
                case 'D':
                    newPosX = curPosX;
                    newPosY = curPosY + 1;
                    break;
                case 'L':
                    newPosX = curPosX - 1;
                    newPosY = curPosY;
                    break;
                case 'R':
                    newPosX = curPosX + 1;
                    newPosY = curPosY;
                    break;
                default:
                    throw new RuntimeException("Unknown " + dir);
            }
            if (newPosX >= 0 && newPosX <= 4 && newPosY >= 0 && newPosY <= 4 &&
                !MATRIX[newPosY][newPosX].isEmpty()
            ) {
                curPosX = newPosX;
                curPosY = newPosY;
            }
        }

//        private void move(char dir) {
//            switch (dir) {
//                case 'U':
//                    if (curNmr > 3) {
//                        curNmr -= 3;
//                    }
//                    return;
//                case 'D':
//                    if (curNmr < 7) {
//                        curNmr += 3;
//                    }
//                    return;
//                case 'L':
//                    if (curNmr % 3 != 1) {
//                        curNmr--;
//                    }
//                    return;
//                case 'R':
//                    if (curNmr % 3 != 0) {
//                        curNmr++;
//                    }
//                    return;
//            }
//            throw new RuntimeException("Unknown " + dir);
//        }
    }
}
